let config = {
  type: Phaser.AUTO,
  width: 800,
  height: 600
}

export const game = new Phaser.Game(config)

game.state.add('load', require('./states/load').loadState)
game.state.add('menu', require('./states/menu').menuState)
game.state.add('play', require('./states/play').playState)

game.state.start('load')
