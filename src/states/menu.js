import {game} from "../game"

export const menuState = {
  create () {
    let nameLabel = game.add.text(80, 80, 'LFP Game', {
      font: '50px Arial',
      fill: '#ffffff'
    })

    let key = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR)
    key.onDown.addOnce(this.start, this)
    this.start() // dev mode only
  },

  start () {
    game.state.start('play')
  }
}