import {game} from "../game"

export const loadState = {
  preload () {
    let label = game.add.text(80, 150, 'Chargement...', {
      font: '30px Helvetica',
      fill: '#ffffff'
    })

    // load all assets here
    game.load.spritesheet('player', require('../assets/player.png'), 128, 128, 6);
    game.load.image('spike', require('../assets/spike2.png'))
    game.load.image('heal', require('../assets/spikered.png'))
    game.load.image('heart', require('../assets/heart.png'))
    game.load.image('heart_stroke', require('../assets/heart_stroke.png'))
    game.load.image('heart_banner', require('../assets/heart_banner.png'))
    game.load.image('spike_banner', require('../assets/spike_banner.png'))
  },
  create () {
    game.state.start('menu')
  }
}