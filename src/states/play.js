import {game} from "../game"

let player,layer1,layer2,coeurs,heals
let vie = 5
let arrows
let cparrow=0
let cpheal=10
let bounds = {
  default: {
    x: 50
  },
  x: {
    min: 20,
    max: 80
  }
}

let hearts, hearts2

export const playState = {
  create () {

    console.log(vie)
    game.physics.startSystem(Phaser.Physics.ARCADE)
    game.physics.arcade.gravity.y = 300

    hearts = game.add.sprite(0, 600 - 50, 'heart_banner')
    hearts.width = 800
    hearts.height = 50
    game.physics.enable(hearts, Phaser.Physics.ARCADE)
    //hearts.body.enable=true
    hearts.body.immovable=true
    hearts.body.moves=false

    hearts2 = game.add.sprite(0, 50, 'heart_banner')
    hearts2.width = 800
    hearts2.height = 50
    hearts2.scale.y *= -1
    game.physics.enable(hearts2, Phaser.Physics.ARCADE)
    //hearts2.body.enable=true
    hearts2.body.immovable=true
    hearts2.body.moves=false


    player = game.add.sprite(bounds.default.x, 600 / 2, 'player')
    player.width = 48
    player.height = 48
    game.physics.enable(player, Phaser.Physics.ARCADE)

    player.body.collideWorldBounds = true

    player.animations.add('right', [0, 1], 5, true);
    player.animations.add('hit', [0, 2, 1, 3], 8, true);
    player.animations.add('happy', [4, 5], 5, true);

    player.animations.play('right')

    // creation des groupes
    coeurs=game.add.group()
    arrows=game.add.group()
    heals=game.add.group()


  },
  update(){

    console.log(vie)
    game.physics.arcade.collide(player, hearts,donnerVie);
    game.physics.arcade.collide(player, hearts2,donnerVie);
    game.physics.arcade.collide(player, heals,donnerVie);
    game.physics.arcade.collide(player, arrows, retirerVie);
    cparrow++
    cpheal++
    if(cparrow==60){
     createArrow()
    }
    if(cpheal==60){
      createHeal()
    }
    afficherCoeur(vie)

    if (game.input.keyboard.isDown(Phaser.Keyboard.Q)) {
      if (player.body.x > bounds.x.min) {
        player.body.velocity.x = -500
      }
      if (player.body.x < bounds.x.min) {
        player.body.x = bounds.x.min
        player.body.velocity.x = 0
      }
    } else if (game.input.keyboard.isDown(Phaser.Keyboard.D)) {
      if (player.body.x < bounds.x.max) {
        player.body.velocity.x = 500
      }
      if (player.body.x > bounds.x.max) {
        player.body.x = bounds.x.max
        player.body.velocity.x = 0
      }
    } else if (game.input.keyboard.isDown(Phaser.Keyboard.Z)) {
      player.body.velocity.y = -300
    } else if (game.input.keyboard.isDown(Phaser.Keyboard.S)) {
      player.body.velocity.y = 300
    } else {
      player.body.velocity.x = 0
      if (player.body.x < bounds.default.x) {
        player.body.x += 1
      } else if (player.body.x > bounds.default.x) {
        player.body.x -= 1
      }
    }


  }
}
function createHeal() {
  heals.callAll('kill')
  let heal=game.add.sprite(600,game.world.randomY,'heal')
  heal.width=30
  heal.height=30
  game.physics.enable(heal, Phaser.Physics.ARCADE)
  heal.body.allowGravity=false
  heal.body.velocity.x=-700
  heals.add(heal)
  cpheal=0

}
function createArrow() {
  arrows.callAll('kill')
  let arrow=game.add.sprite(600,game.world.randomY,'spike')
  arrow.width=30
  arrow.height=30
  game.physics.enable(arrow, Phaser.Physics.ARCADE)
  arrow.body.allowGravity=false
  arrow.body.velocity.x=-700
  arrows.add(arrow)
  cparrow=0

}
function donnerVie() {
  if(vie<5 ){
    vie++
    heals.callAll('kill')


  }

}
function retirerVie(test) {
  if (vie>0){
    vie--
    arrows.callAll('kill')
  }


}
function afficherCoeur(nbcoeur) {
  coeurs.callAll('kill')
  for (let pas=0;pas<nbcoeur;pas++){
    let vie=game.add.sprite(600+(32*pas),50,'heart')
    vie.width=20
    vie.height=30
    coeurs.add(vie)
  }
  for (let pas =0;pas < (5-nbcoeur);pas++){
    let vie=game.add.sprite(600+(32*pas+nbcoeur*32), 50,'heart_stroke')
    vie.width=20
    vie.height=30
    coeurs.add(vie)
  }

}